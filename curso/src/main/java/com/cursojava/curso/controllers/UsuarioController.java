package com.cursojava.curso.controllers;


import com.cursojava.curso.dao.UsuarioDao;
import com.cursojava.curso.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioDao usuarioDao;


    @RequestMapping(value = "api/usuarios", method = RequestMethod.GET)
    public List<Usuario> getUsuarios(){

        return usuarioDao.getUsuarios();
    }


    @RequestMapping(value = "api/usuarios", method = RequestMethod.POST)
    public void registrarUsuarios(@RequestBody Usuario usuario){

        usuarioDao.registrar(usuario);
    }

    @RequestMapping(value = "usuario234")
    public Usuario editar(){
        Usuario usuario = new Usuario();
        usuario.setNombre("Jhon Eduard");
        usuario.setApellido("Agudelo");
        usuario.setEmail("unejemplo123@gmail.com");
        usuario.setTelefono("12323456");
        return   usuario;
    }




    @RequestMapping(value = "api/usuarios/{id}", method = RequestMethod.DELETE)
    public void eliminar(@PathVariable Long id){
        usuarioDao.eliminar(id);
    }



    @RequestMapping(value = "usuario456")
    public Usuario buscar(){
        Usuario usuario = new Usuario();
        usuario.setNombre("Jhon Eduard");
        usuario.setApellido("Agudelo");
        usuario.setEmail("unejemplo123@gmail.com");
        usuario.setTelefono("12323456");
        return   usuario;
    }
}
